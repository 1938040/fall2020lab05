package movies.importer;
import java.util.*;

public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String source, String destination) {
		super(source,destination,true);
	}
	
	public ArrayList<String> process(ArrayList<String> origin) {
		ArrayList<String> asLower=new ArrayList<String>();
		
		for (String i : origin) {
			asLower.add(i.toLowerCase());
		}
		
		return asLower;
	}
}

